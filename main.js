// JavaScript solution to the Alphabet Soup problem
// Solution coded by Bryce Taylor on 3/17/23
// Using JavaScript with Node v18.15.0


var inputFile = "input.txt";

// Find a word's location given the grid
function findWord(grid, word) {
    // Find instance of first letter, then search different directions for rest of the word
    for (var r = 0; r < grid.length; r++) {
        for (var c = 0; c < grid[0].length; c++) {
            if (word[0] == grid[r][c]) {
                // First letter found, now search all 8 directions
                searchDirection(grid, word, r, c, -1, -1);
                searchDirection(grid, word, r, c, -1, 0);
                searchDirection(grid, word, r, c, -1, 1);
                searchDirection(grid, word, r, c, 0, -1);
                searchDirection(grid, word, r, c, 0, 1);
                searchDirection(grid, word, r, c, 1, -1);
                searchDirection(grid, word, r, c, 1, 0);
                searchDirection(grid, word, r, c, 1, 1);
            }
        }
    }
}

// Find rest of word given location of first letter in a certain direction, if possible
function searchDirection(grid, word, r, c, dirR, dirC) {
    for (i = 1; i < word.length; i++) {
        if (r + i * dirR < 0 || r + i * dirR > grid.length-1) {
            return;
        }
        if (c + i * dirC < 0 || c + i * dirC > grid[0].length-1) {
            return;
        }
        if (grid[r + i * dirR][c + i * dirC] != word[i]) {
            return;
        }
    }
    // Output the word along with start and end coordinates
    console.log(word + " " + r + ":" + c + " " + (r + (word.length-1) * dirR) + ":" + (c + (word.length-1) * dirC));
}


// Read the input file to obtain the grid and the words to be found
var fs = require("fs");
fs.readFile(inputFile, "utf8", function(err, data) {

    // Split data into lines
    var lines = data.split("\n");
    for (var i = 0; i < lines.length; i++) {
        lines[i] = lines[i].trim();
    }

    // Ascertain number of rows
    var temp = lines[0].split("x");
    var numRows = temp[0];

    // Create the grid of letters
    var grid = new Array(numRows);
    for (var i = 0; i < numRows; i++) {
        var row = lines[i+1].split(" ");
        grid[i] = row;
    }
    //console.log(grid);

    // Create list of words to find
    var numWords = lines.length - 1 - numRows;
    var words = new Array(numWords);
    for (var i = 0; i < numWords; i++) {
        words[i] = lines[i + 1 + parseInt(numRows)];
    }
    //console.log(words);


    // Start finding and outputting words
    for (var i = 0; i < numWords; i++) {
        findWord(grid, words[i]);
    }
    
});
